import 'dart:math';

import 'package:example/refreshable_view.dart';
import 'package:example/tabs_example.dart';
import 'package:flutter/material.dart';
import 'package:flutter_useful_widgets/flutter_useful_widgets.dart';
import 'package:image_picker/image_picker.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Useful widgets',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Examples(),
    );
  }
}

class Examples extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scafKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scafKey,
        appBar: AppBar(
          title: Text('Useful Widgets'),
          actions: <Widget>[
            getPopupMenu(context),
          ],
        ),
        floatingActionButton: getFloatingActions(context),
        body: SingleChildScrollView(
          child: Center(
            /// Clickable Text
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                ...getFutures(),
                Divider(),
                ...getStreams(),
                Divider(),
                getClickableText(context),
                Divider(),
                getHighlightText(context),
                Divider(),
                getImagePicker(context),
                Divider(),
                RaisedButton(
                  onPressed: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) => RefreshableListViewExample())),
                  child: Text('Refreshable ListView'),
                ),
                RaisedButton(
                  onPressed: () => Navigator.push(context,
                      MaterialPageRoute(builder: (_) => TabsExample())),
                  child: Text('Tabs View'),
                ),
                RaisedButton(
                  onPressed: () => Navigator.push(context,
                      MaterialPageRoute(builder: (_) => MySearchView())),
                  child: Text('Search View'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  ClickableText getClickableText(BuildContext context) {
    return ClickableText(
      text: 'Show BottomSheet',

      /// Bottom Sheet
      onTap: () => BottomSheetDialog.show(
        context: context,
        dialog: getBottomSheetDialog(context),
      ),
    );
  }

  BottomSheetDialog getBottomSheetDialog(BuildContext context) {
    return BottomSheetDialog(
      items: [
        BottomSheetItem(
          title: Text('Flutter'),
          onTap: () => showSnackBar('Flutter'),
        ),
        BottomSheetItem(
          title: Text('Android'),
          onTap: () => showSnackBar('Android'),
        ),
        BottomSheetItem(
          title: Text('Iphone'),
          onTap: () => showSnackBar('Iphone'),
        ),
      ],
    );
  }

  List<Widget> getFutures() {
    return [
      Text('Future Parser'),

      /// Future Parser
      FutureParser(
        initialData: 'Initial value',
        future: getFutureString('With initial value'),
        builder: (context, value) => Text(value),
      ),
      FutureParser(
        future: getFutureString('String from future'),
        builder: (context, value) => Text(value),
      ),
      FutureParser(
        future: getFutureString(null),
        builder: (context, value) => Text(value),
      ),
      FutureParser(
        future: getFutureString('Error', exception: true),
        builder: (context, value) => Text(value),
      ),
      FutureParserConverter<String, int>(
        initialData: 10,
        future: getFutureString('1'),
        converter: (value) => int.parse(value),
        builder: (context, value) => Text('$value'),
      ),
      FutureParserConverter<String, int>(
        future: getFutureString('1'),
        converter: (value) => int.parse(value),
        builder: (context, value) => Text('$value'),
      ),
      FutureParserConverter<String, int>(
        future: getFutureString('1'),
        converter: (value) => int.parse(value),
        builder: (context, value) => Text('$value'),
      ),
      FutureParserConverter<String, int>(
        future: getFutureString(null),
        converter: (value) => int.parse(value),
        builder: (context, value) => Text('$value'),
      ),
      FutureParserConverter<String, int>(
        future: getFutureString('error'),
        converter: (value) => int.parse(value),
        builder: (context, value) => Text('$value'),
      ),
    ];
  }

  List<Widget> getStreams() {
    return [
      Text('Stream Parser'),
      StreamParser<int>(
          initialData: 1000,
          stream: getStream(),
          builder: (context, value) => Text('$value')),
      StreamParser<int>(
          stream: getStream(), builder: (context, value) => Text('$value')),
      StreamParser<int>(
          stream: getStream(empty: true),
          builder: (context, value) => Text('$value')),
      StreamParser<int>(
          stream: getStream(exception: true),
          builder: (context, value) => Text('$value')),
    ];
  }

  Future<String> getFutureString(String value, {bool exception = false}) async {
    await Future.delayed(Duration(seconds: 5));
    if (exception) {
      throw 'Exception for: $value';
    }
    return value;
  }

  Stream<int> getStream({bool exception = false, bool empty = false}) {
    return Stream<int>.periodic(Duration(seconds: 1), (x) {
      Random random = Random();
      if (exception && random.nextBool()) {
        throw 'Exception for stream: ${x + 1}';
      }
      if (empty && random.nextBool()) {
        return null;
      }
      return x + 1;
    }).take(15);
  }

  void showSnackBar(String text) {
    _scafKey.currentState.showSnackBar(
      SnackBar(
        content: Text(text),
      ),
    );
  }

  HighlightedSearchText getHighlightText(BuildContext context) {
    return HighlightedSearchText(
      text: 'Visit our terms and conditions',
      queries: [
        TextQuery(
          query: 'terms',
          style: TextStyle(color: Colors.red),
          onTap: () => showSnackBar('Terms'),
        ),
        TextQuery(
          query: 'conditions',
          style: TextStyle(color: Colors.green),
        ),
      ],
    );
  }

  ImagePickerDialog getImagePicker(BuildContext context) {
    return ImagePickerDialog(
      title: 'Pick an image',
      initialUrl:
          'https://firebasestorage.googleapis.com/v0/b/efootball-6e071.appspot.com/o/communities%2FBfEN3MFsA1PokqCgZfNf?alt=media&token=4aded483-68be-4941-8e88-0e44ffe27f7f',
      height: 100,
      onChange: (file) => null,
      onPickImage: (source, quality) async => await ImagePicker.pickImage(
          source: source == ImagePickerDialog.Gallery
              ? ImageSource.gallery
              : ImageSource.camera),
    );
  }

  Widget getFloatingActions(BuildContext context) {
    return MultiFloatingActionButton(
      mini: false,
      actions: [
        FloatingAction(
          child: Icon(Icons.add),
          onTap: () => showSnackBar('Add'),
        ),
        FloatingAction(
          child: Icon(Icons.edit),
          onTap: () => showSnackBar('Edit'),
        ),
      ],
    );
  }

  Widget getPopupMenu(BuildContext context) {
    return PopupMenu(
      items: [
        PopupItem(
          onTap: () => showSnackBar('Edit'),
          child: Text('Edit'),
        ),
        TextPopupItem(
          onTap: () => showSnackBar('Settings'),
          value: 'Settings',
          icon: Icon(
            Icons.settings,
            color: Theme.of(context).colorScheme.onSurface,
          ),
        ),
      ],
    );
  }
}

class MySearchView extends StatelessWidget {
  final GlobalKey<SearchViewState> _searchKey = GlobalKey();
  final values = ['Flutter', 'Android', 'Iphone', 'Web', 'Angular'];
  @override
  Widget build(BuildContext context) {
    return SearchView<String>(
      key: _searchKey,
      initialValues: () => values,
      fetchResults: (query) async {
        await Future.delayed(Duration(seconds: 1));
        return values
            .where((v) => v.toLowerCase().contains(query.toLowerCase()))
            .toList();
      },
      resultsFilter: (query, results) => results
          .where((value) => value.toLowerCase().contains(query.toLowerCase()))
          .toList(),
      title: Text('Search Text'),
      itemBuilder: (context, value, query) {
        return ListTile(
          onTap: () => itemClick(value),
          title: HighlightedSearchText(
            text: value,
            ignoreCase: true,
            queries: [
              TextQuery(
                query: query,
              )
            ],
          ),
        );
      },
    );
  }

  void itemClick(String value) {
    _searchKey.currentState.scaffoldState
        .showSnackBar(SnackBar(content: Text(value)));
  }
}
