import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_useful_widgets/flutter_useful_widgets.dart';

class TabsExample extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return TabsView(
      useSafeArea: true,
      automaticallyImplyLeading: true,
      title: Text('Title'),
      centerTitle: true,
      textColor: Colors.red,
      barColor: Colors.green,
      actions: <Widget>[Icon(Icons.edit)],
      floatingActionButton: FloatingActionButton(
        onPressed: () => null,
        child: Icon(Icons.edit),
      ),
      bottomNavigationBar: BottomNavigationBar(items: [
        BottomNavigationBarItem(icon: Icon(Icons.home), title: Text('Home')),
        BottomNavigationBarItem(
            icon: Icon(Icons.person), title: Text('Profile')),
      ]),
      tabs: getTabs(context),
    );
  }

  getTabs(BuildContext context) {
    return [
      TabItem(
        tab: Tab(child: Text("Home"), icon: Icon(Icons.home),),
        view: getMultiPages(),
      ),
      TabItem(
        tab: Tab(child: Text("Profile"), icon: Icon(Icons.person),),
        view: Center(
          child: Text('Profile'),
        ),
      ),
    ];
  }

  Widget getMultiPages() {
    return MultiPages(
      pages: [
        Center(
          child: Text('Page 1'),
        ),
        Center(
          child: Text('Page 2'),
        ),
      ],
    );
  }
}
