import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_useful_widgets/flutter_useful_widgets.dart';

class RefreshableListViewExample extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Refreshable List View'),
        ),
        body: getRefreshableListView(context),
      ),
    );
  }

  Widget getRefreshableListView(BuildContext context) {
    return RefreshableListView<String>(
      onRefresh: () async {
        Future.delayed(Duration(seconds: 3));
        return getRandomStrings();
      },
      items: getRandomStrings(),
      itemBuilder: (context, item) {
        return Text(item);
      },
    );
  }

  List<String> getRandomStrings() {
    Random r = Random();
    return List.generate(r.nextInt(15) + 1, (i) => 'Item ${i + 1}').toList();
  }
}
