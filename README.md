# useful_widgets

Useful widgets for speed up development process and reduce code boilerplate.
## Widgets
**StreamParser<T<T>>** - wrapper around **StreamBuilder** that provides a builder for each connection state.
**StreamParserConverter<S<T>,D>** - wrapper around **StreamBuilder** that converts a stream of **S** to a stream of **D**.
**FutureParser<T<T>>** - wrapper around **FutureBuilder** that provides a builder for each connection state.
**FutureParserConverter<S<T>,D>** - wrapper around **FutureBuilder** that converts a future of **S** to a future of **D**.
**BottomSheetDialog** - show a list of **BottomSheetItem**.
**HighlightedSearchText** - show a list of **TextQuery** formatted on a given text.
**ImagePickerDialog** - show image from network or file and when clicked it shows options to load from camera, gallery or delete current image if any. The real picker is chosen by the user on the **onPickImage** function, in the example project it was used the **image_picker: ^0.6.1+8** you can used a package that feet your needs.
**MultiFloatingActionButton** - show multiple **FloatingAction** with animations.
**MultiPages** - wrapper around **PageView** that shows the pages indicator (a bar and a dot for each page with the current page highlighted).
**PopupMenu<T<T>>** - easy to use **PopupMenuButton** with **PopupItem<T<T>>** and **TextPopupItem**.
**RefreshableListView<T<t>>** - a ListView with RefreshIndicator and the **onRefresh** returns a **List<T<t>>** items.
**SearchView<T<t>>** - provides functions to reduce boilerplate code and improve development time like: initialValues, fetchResults(query), resultsFilter(query, results), itemBuilder(context, item, query).
**TabsView** - provides a scaffold with TabBar and TabView that are passed as a list of **TabItem**.

## Getting Started

This project was created by **Edgar Wchua P. Guilherme** (<wchua043@gmail.com>) as an open source project at [gitlab](https://gitlab.com/EdgarG/useful_widgets).
To understand how to use the widgets please check the [examples](https://gitlab.com/EdgarG/useful_widgets/-/tree/master/example).