## [1.0.0] - Released at 15/02/2020.

* This release contains 14 widgets, To understand how to use the widgets please check the [examples](https://gitlab.com/EdgarG/useful_widgets/-/tree/master/example).

## [1.1.0] - Change on class structure

## [1.1.0+1] - Changes on the TabsView widget to include Tab on the TabItem
