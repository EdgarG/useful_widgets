import 'package:flutter/material.dart';

///
class SearchView<T> extends StatefulWidget {
  final List<T> Function() initialValues;
  final Future<List<T>> Function(String query) fetchResults;
  final List<T> Function(String query, List<T> results) resultsFilter;
  final Widget Function(BuildContext context, T item, String query) itemBuilder;

  final Widget title;
  final String searchHint;
  final Icon searchingIcon;
  final Icon clearIcon;

  final bool useSafeArea;

  const SearchView({
    Key key,
    this.initialValues,
    @required this.resultsFilter,
    @required this.title,
    @required this.itemBuilder,
    this.fetchResults,
    this.searchHint,
    this.searchingIcon,
    this.clearIcon,
    this.useSafeArea = true,
  })  : assert(resultsFilter != null),
        assert(itemBuilder != null),
        super(key: key);

  @override
  SearchViewState<T> createState() => SearchViewState<T>();
}

class SearchViewState<T> extends State<SearchView<T>> {
  final TextEditingController _textEditingController = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();

  bool _isSearching = false;
  bool _isFecthing = false;
  List<T> _searchedValues = [];
  List<T> _filteredResults = [];

  ScaffoldState get scaffoldState => _scaffoldKey.currentState;

  @override
  void initState() {
    _textEditingController.addListener(() async {
      String query = _textEditingController.text;
      if (query == null || query.isEmpty) {
        _setInitialValue();
      } else {
        if (query != null &&
            query.isNotEmpty &&
            query.length == 1 &&
            widget.fetchResults != null) {
          setState(() {
            _isFecthing = true;
          });

          var results = await widget.fetchResults(query);
          _updateSearchedValuesAndFilters(results);

          setState(() {
            _isFecthing = false;
          });
        } else {
          var results = widget.resultsFilter(query, _searchedValues);
          _updateFilteredResults(results);
        }
      }
    });

    _setInitialValue();

    super.initState();
  }

  void _setInitialValue() {
    var results = _getInitialValue();
    _updateSearchedValuesAndFilters(results);
  }

  void _updateSearchedValuesAndFilters(List<T> results) {
    if (mounted) {
      setState(() {
        _searchedValues = results;
        _filteredResults = results;
      });
    } else {
      _searchedValues = results;
      _filteredResults = results;
    }
  }

  void _updateFilteredResults(List<T> results) {
    if (mounted) {
      setState(() {
        _filteredResults = results;
      });
    } else {
      _filteredResults = results;
    }
  }

  List<T> _getInitialValue() {
    if (widget.initialValues != null) {
      return widget.initialValues();
    }
    return [];
  }

  @override
  Widget build(BuildContext context) {
    var scaffold = Scaffold(
      key: _scaffoldKey,
      appBar: _getAppBar(),
      body: _getBody(),
    );

    if (widget.useSafeArea) {
      return SafeArea(
        child: scaffold,
      );
    }
    return scaffold;
  }

  Widget _getAppBar() {
    if (_isSearching) {
      return AppBar(
        actions: [
          IconButton(
              icon: widget.clearIcon ?? Icon(Icons.clear),
              onPressed: () {
                _textEditingController.clear();
                setState(() {
                  _isSearching = false;
                });
              })
        ],
        title: TextField(
          controller: _textEditingController,
          style: TextStyle(color: Theme.of(context).colorScheme.onPrimary),
          decoration: InputDecoration(
            hintText: widget.searchHint ?? 'Search',
            hintStyle:
                TextStyle(color: Theme.of(context).colorScheme.onPrimary),
          ),
        ),
      );
    }

    return AppBar(
      actions: [
        IconButton(
          icon: widget.searchingIcon ?? Icon(Icons.search),
          onPressed: () => setState(() => _isSearching = true),
        )
      ],
      title: InkWell(
        child: widget.title ?? Text('Search'),
        onTap: () => setState(() => _isSearching = true),
      ),
    );
  }

  Widget _getBody() {
    if (_isFecthing) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }

    if (_filteredResults == null || _filteredResults.isEmpty) {
      return Center(child: Text('No results!'));
    }
    return ListView(
      children: _filteredResults
          .map((item) =>
              widget.itemBuilder(context, item, _textEditingController.text))
          .toList(),
    );
  }
}
