import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class TextQuery {
  final String query;
  final TextStyle style;
  final VoidCallback onTap;

  TextQuery({
    @required this.query,
    this.style,
    this.onTap,
  }) : assert(query != null);
}

class HighlightedSearchText extends StatelessWidget {
  final String text;
  final List<TextQuery> queries;
  final TextStyle normalStyle;
  final TextAlign textAlign;
  final bool ignoreCase;

  final TextStyle _defaultNormalStyle = const TextStyle(color: Colors.grey);
  final TextStyle _defaultHighlightStyle =
      const TextStyle(fontWeight: FontWeight.bold, color: Colors.black);

  const HighlightedSearchText({
    Key key,
    @required this.text,
    @required this.queries,
    this.normalStyle,
    this.textAlign,
    this.ignoreCase = false,
  })  : assert(text != null),
        assert(queries != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return RichText(
      textAlign: textAlign ?? TextAlign.start,
      text: TextSpan(
        children: _getHighlightedResults(),
        style: normalStyle ?? _defaultNormalStyle,
      ),
    );
  }

  bool _textContainsQuery(String text, List<TextQuery> queries,
      [bool caseSensitive = false]) {
    for (TextQuery q in queries) {
      if (caseSensitive && text.contains(q.query)) {
        return true;
      }
      if (!caseSensitive &&
          text.toLowerCase().contains(q.query.toLowerCase())) {
        return true;
      }
    }

    return false;
  }

  List<Match> _getMatches(String text, List<TextQuery> queries,
      [bool caseSensitive = false]) {
    List<Match> matches = [];

    for (TextQuery q in queries) {
      if (caseSensitive) {
        var allMatches = q.query.allMatches(text).toList();
        matches.addAll(allMatches);
      } else {
        var allMatches =
            q.query.toLowerCase().allMatches(text.toLowerCase()).toList();
        matches.addAll(allMatches);
      }
    }

    return matches;
  }

  List<TextSpan> _getHighlightedResults() {
    if (queries == null ||
        queries.isEmpty ||
        !_textContainsQuery(text, queries, !ignoreCase)) {
      return [TextSpan(text: text)];
    }

    final List<Match> matches = _getMatches(text, queries, !ignoreCase);

    final List<TextSpan> children = [];

    int lastMatchEnd = 0;
    for (var i = 0; i < matches.length; i++) {
      final match = matches.elementAt(i);

      if (match.start != lastMatchEnd) {
        children.add(
          TextSpan(
            text: text.substring(lastMatchEnd, match.start),
            style: normalStyle ?? _defaultNormalStyle,
          ),
        );
      }

      var queryText = text.substring(match.start, match.end);
      var textQuery = getQueryByText(queryText);

      children.add(
        TextSpan(
          text: queryText,
          style: textQuery?.style ?? _defaultHighlightStyle,
          recognizer: TapGestureRecognizer()..onTap = textQuery.onTap,
        ),
      );

      if (i == matches.length - 1 && match.end != text.length) {
        children.add(
          TextSpan(
            text: text.substring(match.end, text.length),
            style: normalStyle ?? _defaultNormalStyle,
          ),
        );
      }

      lastMatchEnd = match.end;
    }

    return children;
  }

  TextQuery getQueryByText(String text) {
    return queries.firstWhere((q) {
      if (ignoreCase) {
        return q.query.toLowerCase() == text.toLowerCase();
      }
      return q.query == text;
    }, orElse: null);
  }
}
