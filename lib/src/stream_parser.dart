import 'package:flutter/material.dart';

/// This class is a wrapper around [StreamBuilder] that return widgets for
/// every connection states, errors and no data edge cases.
class StreamParser<T> extends StatelessWidget {
  final Stream<T> stream;
  final T initialData;
  final Widget Function(BuildContext context, T data) builder;
  final Widget Function(BuildContext context, Object error) errorBuilder;
  final Widget Function(BuildContext context) loadingBuilder;
  final Widget Function(BuildContext context) noDataBuilder;

  const StreamParser({
    Key key,
    @required this.stream,
    @required this.builder,
    this.initialData,
    this.errorBuilder,
    this.loadingBuilder,
    this.noDataBuilder,
  })  : assert(stream != null),
        assert(builder != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<T>(
        stream: stream,
        builder: (BuildContext context, AsyncSnapshot<T> snapshot) {
          if (snapshot.hasError) {
            if (errorBuilder == null) {
              return Container(
                color: Theme.of(context).scaffoldBackgroundColor,
                child: Center(
                  child: Text(
                    "${snapshot.error}",
                    style:
                        TextStyle(color: Theme.of(context).colorScheme.error),
                  ),
                ),
              );
            }

            return errorBuilder(context, snapshot.error);
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            if(initialData != null){
              return builder(context, initialData);
            }

            if (loadingBuilder == null) {
              return Container(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  child: Center(child: CircularProgressIndicator()));
            }

            return loadingBuilder(context);
          }

          bool isIterable = snapshot.data is Iterable;

          if (snapshot.hasData == false ||
              (isIterable && (snapshot.data as Iterable).isEmpty)) {
            if (noDataBuilder == null) {
              return Container(
                color: Theme.of(context).scaffoldBackgroundColor,
                child: Center(child: Text('No data from the stream.')),
              );
            }

            return noDataBuilder(context);
          }

          return builder(context, snapshot.data);
        });
  }
}

/// This class is a wrapper around [StreamBuilder] that return widgets for
/// every connection states, errors and no data edge cases.
/// It is useful for convert for example a [Stream] of firebase [QuerySnapshot]
/// to a Stream of [List]
///
class StreamParserConverter<SourceType, DestinyType> extends StatelessWidget {
  final Stream<SourceType> stream;
  final DestinyType initialData;
  final DestinyType Function(SourceType data) converter;
  final Widget Function(BuildContext context, DestinyType data) builder;
  final Widget Function(BuildContext context, Object error) errorBuilder;
  final Widget Function(BuildContext context) loadingBuilder;
  final Widget Function(BuildContext context) noDataBuilder;

  const StreamParserConverter({
    Key key,
    @required this.stream,
    @required this.converter,
    @required this.builder,
    this.initialData,
    this.errorBuilder,
    this.loadingBuilder,
    this.noDataBuilder,
  })  : assert(stream != null),
        assert(converter != null),
        assert(builder != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<SourceType>(
        stream: stream,
        builder: (BuildContext context, AsyncSnapshot<SourceType> snapshot) {
          if (snapshot.hasError) {
            return _buildError(context, snapshot.error);
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            if(initialData != null){
              return builder(context, initialData);
            }

            if (loadingBuilder == null) {
              return Container(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  child: Center(child: CircularProgressIndicator()));
            }

            return loadingBuilder(context);
          }

          bool isIterable = snapshot.data is Iterable;

          if (snapshot.hasData == false ||
              (isIterable && (snapshot.data as Iterable).isEmpty)) {
            if (noDataBuilder == null) {
              return Container(
                color: Theme.of(context).scaffoldBackgroundColor,
                child: Center(child: Text('No data from the stream.')),
              );
            }

            return noDataBuilder(context);
          }

          try {
            var data = converter(snapshot.data);
            return builder(context, data);
          }catch(error){
            return _buildError(context, error);
          }

        });
  }

  Widget _buildError(BuildContext context, var error){
    if (errorBuilder == null) {
      return Container(
        color: Theme.of(context).scaffoldBackgroundColor,
        child: Center(
          child: Text(
            "$error",
            style:
            TextStyle(color: Theme.of(context).colorScheme.error),
          ),
        ),
      );
    }

    return errorBuilder(context, error);
  }

}
