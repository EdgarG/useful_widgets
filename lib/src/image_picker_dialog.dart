import 'dart:io';

import 'package:flutter/material.dart';

/// This dialog show options to load an image from gallary, camera or delete the
/// current image file
///
/// The image picker itself should be passed on onPickImage function
///
class ImagePickerDialog extends StatefulWidget {
  final String title;
  final String initialUrl;
  final int compressQuality;
  final void Function(File file) onChange;
  final Future<File> Function(String source, int compressQuality) onPickImage;
  final EdgeInsetsGeometry padding;
  final double width;
  final double height;
  final Color iconColor;

  static const String Camera = 'camera';
  static const String Gallery = 'gallery';

  const ImagePickerDialog({
    Key key,
    this.title,
    this.initialUrl,
    this.compressQuality = 75,
    this.padding,
    this.width,
    this.height,
    this.iconColor,
    @required this.onChange,
    @required this.onPickImage,
  })  : assert(onChange != null),
        assert(onPickImage != null),
        super(key: key);

  @override
  _ImagePickerDialogState createState() => _ImagePickerDialogState();
}

class _ImagePickerDialogState extends State<ImagePickerDialog> {
  File file;
  bool changed;
  String url;
  Color color;
  double iconSize;
  double width;
  double height;

  @override
  void initState() {
    changed = false;
    url = widget.initialUrl;
    color = widget.iconColor ?? Colors.grey;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    iconSize = Theme.of(context).iconTheme.size ?? 28;
    width = widget.width ?? MediaQuery.of(context).size.width;
    height = (widget.height ?? iconSize) * 2;

    return Card(
      elevation: 2.0,
      child: Container(
        width: width,
        height: height,
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            getForeground(),
            getCameraIcon(),
          ],
        ),
      ),
    );
  }

  Widget getForeground() {
    if (url == null && file == null) {
      return Container(
        width: width,
        height: height,
      );
    }

    return Container(
      width: width,
      height: height,
      child: Image(
        image: file == null ? NetworkImage(url) : FileImage(file),
        fit: BoxFit.fill,
      ),
    );
  }

  Widget getCameraIcon() {
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Center(
          child: Container(
            color: color.withOpacity(0.2),
            width: iconSize * 1.1,
            height: iconSize * 1.1,
          ),
        ),
        InkWell(
          child: Icon(Icons.camera_alt),
          onTap: () async => await loadImage(),
        )
      ],
    );
  }

  Future loadImage() async {
    List<_Action> actions = [
      _Action(
          text: 'Camera',
          iconData: Icons.camera_alt,
          onTap: () => loadImageFrom(ImagePickerDialog.Camera)),
      _Action(
          text: 'Gallery',
          iconData: Icons.image,
          onTap: () => loadImageFrom(ImagePickerDialog.Gallery)),
    ];

    if (file != null || url != null) {
      actions.add(
        _Action(
            text: 'Delete',
            iconData: Icons.clear,
            onTap: () => deletePicture()),
      );
    }

    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return AlertDialog(
            title: Text(widget.title ?? 'Select the action'),
            content: SingleChildScrollView(
              child: ListBody(
                children: actions,
              ),
            ),
          );
        });
  }

  Future loadImageFrom(String source) async {
    Navigator.of(context).pop();

    var pickedFile = await widget.onPickImage(source, widget.compressQuality);

    if (pickedFile != null) {
      setState(() {
        file = pickedFile;
        changed = true;
      });

      widget.onChange(file);
    }
  }

  Future deletePicture() async {
    Navigator.of(context).pop();

    setState(() {
      url = null;
      file = null;
      changed = true;
    });

    widget.onChange(file);
  }
}

class _Action extends StatelessWidget {
  final String text;
  final IconData iconData;
  final VoidCallback onTap;

  const _Action(
      {Key key,
      @required this.text,
      @required this.iconData,
      @required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(iconData),
      onTap: onTap,
      title: Text(text),
    );
  }
}
