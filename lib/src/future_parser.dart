import 'package:flutter/material.dart';

/// This class is a wrapper around [FutureBuilder] that return widgets for
/// every connection states, errors and no data edge cases.
class FutureParser<T> extends StatelessWidget {
  final Future<T> future;
  final T initialData;
  final Widget Function(BuildContext context, T data) builder;
  final Widget Function(BuildContext context, Object error) errorBuilder;
  final Widget Function(BuildContext context) loadingBuilder;
  final Widget Function(BuildContext context) noDataBuilder;

  const FutureParser({
    Key key,
    @required this.future,
    @required this.builder,
    this.initialData,
    this.errorBuilder,
    this.loadingBuilder,
    this.noDataBuilder,
  })  : assert(future != null),
        assert(builder != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<T>(
        initialData: initialData,
        future: future,
        builder: (BuildContext context, AsyncSnapshot<T> snapshot) {
          if (snapshot.hasError) {
            if (errorBuilder == null) {
              return Container(
                color: Theme.of(context).scaffoldBackgroundColor,
                child: Center(
                  child: Text(
                    "${snapshot.error}",
                    style:
                        TextStyle(color: Theme.of(context).colorScheme.error),
                  ),
                ),
              );
            }

            return errorBuilder(context, snapshot.error);
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            if (initialData != null) {
              return builder(context, initialData);
            }

            if (loadingBuilder == null) {
              return Container(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  child: Center(child: CircularProgressIndicator()));
            }

            return loadingBuilder(context);
          }

          bool isIterable = snapshot.data is Iterable;

          if (snapshot.hasData == false ||
              (isIterable && (snapshot.data as Iterable).isEmpty)) {
            if (noDataBuilder == null) {
              return Container(
                color: Theme.of(context).scaffoldBackgroundColor,
                child: Center(child: Text('No data from the future.')),
              );
            }

            return noDataBuilder(context);
          }

          return builder(context, snapshot.data);
        });
  }
}

/// This class is a wrapper around [FutureBuilder] that return widgets for
/// every connection states, errors and no data edge cases.
/// It is useful for convert for example a [Future] of firebase [QuerySnapshot]
/// to a Future of [List]
///
class FutureParserConverter<SourceType, DestinyType> extends StatelessWidget {
  final Future<SourceType> future;
  final DestinyType initialData;
  final DestinyType Function(SourceType data) converter;
  final Widget Function(BuildContext context, DestinyType data) builder;
  final Widget Function(BuildContext context, Object error) errorBuilder;
  final Widget Function(BuildContext context) loadingBuilder;
  final Widget Function(BuildContext context) noDataBuilder;

  const FutureParserConverter({
    Key key,
    @required this.future,
    @required this.converter,
    @required this.builder,
    this.initialData,
    this.errorBuilder,
    this.loadingBuilder,
    this.noDataBuilder,
  })  : assert(future != null),
        assert(converter != null),
        assert(builder != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<SourceType>(
        future: future,
        builder: (BuildContext context, AsyncSnapshot<SourceType> snapshot) {
          if (snapshot.hasError) {
            return _buildError(context, snapshot.error);
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            if (initialData != null) {
              return builder(context, initialData);
            }

            if (loadingBuilder == null) {
              return Container(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  child: Center(child: CircularProgressIndicator()));
            }

            return loadingBuilder(context);
          }

          bool isIterable = snapshot.data is Iterable;

          if (snapshot.hasData == false ||
              (isIterable && (snapshot.data as Iterable).isEmpty)) {
            if (noDataBuilder == null) {
              return Container(
                color: Theme.of(context).scaffoldBackgroundColor,
                child: Center(child: Text('No data from the future.')),
              );
            }

            return noDataBuilder(context);
          }

          try {
            var data = converter(snapshot.data);
            return builder(context, data);
          } catch (error) {
            return _buildError(context, error);
          }
        });
  }

  Widget _buildError(BuildContext context, var error) {
    if (errorBuilder == null) {
      return Container(
        color: Theme.of(context).scaffoldBackgroundColor,
        child: Center(
          child: Text(
            "$error",
            style: TextStyle(color: Theme.of(context).colorScheme.error),
          ),
        ),
      );
    }

    return errorBuilder(context, error);
  }
}
