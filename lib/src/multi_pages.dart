import 'package:flutter/material.dart';

/// Shows a [PageView] with pages indicator
class MultiPages extends StatefulWidget {
  final List<Widget> pages;
  final bool showIndicator;
  final bool showIndicatorBar;
  final Color indicatorColor;
  final Color indicatorBarColor;
  final int indicatorBarHeight;
  final int indicatorSize;
  final double indicatorPadding;
  final Duration animationDuration;
  final Curve curve;
  final EdgeInsets barPadding;

  const MultiPages({
    Key key,
    @required this.pages,
    this.showIndicator = true,
    this.showIndicatorBar = true,
    this.indicatorBarHeight,
    this.indicatorSize,
    this.indicatorColor,
    this.indicatorBarColor,
    this.indicatorPadding,
    this.curve,
    this.animationDuration,
    this.barPadding,
  })  : assert(pages != null),
        assert(pages.length > 0),
        super(key: key);

  @override
  _MultiPagesState createState() => _MultiPagesState();
}

class _MultiPagesState extends State<MultiPages> {
  final PageController controller = PageController();
  int currentIndex = 0;
  double indicatorSize;
  double indicatorBarHeight;

  final double kPageIndicatorSize = 10;
  final double kPageIndicatorBarHeight = 20;

  @override
  void initState() {
    indicatorSize = widget.indicatorSize ?? kPageIndicatorSize;
    indicatorBarHeight = widget.indicatorBarHeight ?? kPageIndicatorBarHeight;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> children = [
      PageView(
        controller: controller,
        onPageChanged: (int index) {
          setState(() {
            currentIndex = index;
          });
        },
        children: widget.pages.map((p) => convertPage(p)).toList(),
      ),
    ];

    if (widget.showIndicator) {
      children.add(getIndicator());
    }

    return Stack(
      children: children,
    );
  }

  Widget convertPage(Widget page) {
    List<Widget> children = [];

    if (widget.showIndicatorBar) {
      children.add(
        Container(
          height: indicatorBarHeight,
        ),
      );
    }

    children.add(Expanded(child: page));
    //children.add(page);

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: children,
    );
  }

  Widget getIndicator() {
    var size = MediaQuery.of(context).size;
    int nIndicator = widget.pages.length;
    var color = widget.indicatorColor ?? Theme.of(context).iconTheme.color;

    return Positioned(
      left: widget.barPadding?.left ?? 0,
      right: widget.barPadding?.right ?? 0,
      top: widget.barPadding?.top ?? 0,
      child: Container(
        width: size.width,
        color: widget.indicatorBarColor ??
            Theme.of(context).colorScheme.onSurface.withOpacity(0.2),
        height: indicatorSize * 2,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: List.generate(nIndicator, (i) {
            return Padding(
              padding: EdgeInsets.symmetric(
                horizontal: widget.indicatorPadding ?? 5,
              ),
              child: InkWell(
                onTap: () {
                  setState(() {
                    controller.animateToPage(
                      i,
                      duration: widget.animationDuration ??
                          Duration(milliseconds: 300),
                      curve: widget.curve ?? Curves.ease,
                    );
                  });
                },
                child: Container(
                  width: indicatorSize,
                  height: indicatorSize,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(indicatorSize / 2),
                    color: i == currentIndex ? color : color.withOpacity(0.5),
                  ),
                ),
              ),
            );
          }).toList(),
        ),
      ),
    );
  }
}
