import 'package:flutter/material.dart';

class ClickableText extends StatelessWidget {
  final String text;
  final VoidCallback onTap;
  final TextStyle style;

  const ClickableText({
    Key key,
    @required this.text,
    @required this.onTap,
    this.style,
  })  : assert(text != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        child: Text(
          text,
          style: style,
        ),
        onTap: onTap,
      ),
    );
  }
}
