import 'package:flutter/material.dart';

class TabItem {
  final Tab tab;
  final Widget view;

  TabItem({
    @required this.tab,
    @required this.view,
  });
}

class TabsView extends StatelessWidget {
  final List<TabItem> tabs;
  final Widget floatingActionButton;
  final Color barColor;
  final Color textColor;
  final Widget title;
  final List<Widget> actions;
  final Widget bottomNavigationBar;
  final Widget bottomSheet;
  final Widget drawer;
  final Widget endDrawer;
  final bool centerTitle;
  final bool useSafeArea;
  final bool automaticallyImplyLeading;

  const TabsView({
    Key key,
    @required this.tabs,
    this.floatingActionButton,
    this.barColor,
    this.textColor,
    this.title,
    this.actions,
    this.bottomNavigationBar,
    this.bottomSheet,
    this.drawer,
    this.endDrawer,
    this.centerTitle = false,
    this.useSafeArea = false,
    this.automaticallyImplyLeading = true,
  })  : assert(tabs != null),
        assert(tabs.length > 0),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isTitle = title == null && (actions == null || actions.isEmpty);

    var tabbar = TabBar(
      tabs: tabs.map((item) => item.tab).toList(),
    );

    var child = DefaultTabController(
      length: tabs.length,
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: automaticallyImplyLeading,
          backgroundColor: barColor ?? Theme.of(context).appBarTheme.color,
          title: isTitle ? tabbar : title,
          centerTitle: !isTitle && centerTitle,
          bottom: isTitle ? null : tabbar,
          actions: actions,
        ),
        body: TabBarView(
          children: tabs.map((item) => item.view).toList(),
        ),
        floatingActionButton: floatingActionButton,
        bottomNavigationBar: bottomNavigationBar,
        bottomSheet: bottomSheet,
        drawer: drawer,
        endDrawer: endDrawer,
      ),
    );

    if (useSafeArea == true) {
      return SafeArea(
        child: child,
      );
    }

    return child;
  }
}
