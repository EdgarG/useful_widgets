import 'package:flutter/material.dart';
import 'dart:math' as math;

/// An action for the [FloatingActionButton]
class FloatingAction {
  Widget child;
  VoidCallback onTap;

  FloatingAction({
    @required this.child,
    @required this.onTap,
  });
}

/// This widget provides an list of actions that shows up when the
/// [FloatingActionButton] is pressed
class MultiFloatingActionButton extends StatefulWidget {
  final Widget dismissed;
  final Widget active;
  final List<FloatingAction> actions;
  final Duration animationDuration;
  final Curve curve;
  final bool mini;
  final Color color;

  const MultiFloatingActionButton({
    Key key,
    this.dismissed = const Icon(Icons.more_vert),
    this.active = const Icon(Icons.close),
    @required this.actions,
    this.animationDuration,
    this.curve,
    this.mini,
    this.color,
  })  : assert(actions != null),
        assert(actions.length > 0),
        super(key: key);

  @override
  _MultiFloatingActionButtonState createState() =>
      _MultiFloatingActionButtonState();
}

class _MultiFloatingActionButtonState extends State<MultiFloatingActionButton>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;

  double height;
  double width = 56;

  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: widget.animationDuration ?? const Duration(milliseconds: 500),
    );

    height = widget.mini ? null : 60;

    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final actions = widget.actions;
    final color = widget.color ?? Theme.of(context).colorScheme.secondary;

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: List<Widget>.generate(actions.length, (int index) {
        return Container(
          height: height,
          width: width,
          alignment: FractionalOffset.topCenter,
          child: ScaleTransition(
            scale: CurvedAnimation(
                parent: _animationController,
                curve: Interval(
                  0.0,
                  1.0 - index / actions.length / 2.0,
                  curve: widget.curve ?? Curves.easeOut,
                )),
            child: FloatingActionButton(
              heroTag: "Action_$index",
              backgroundColor: color.withOpacity(0.7),
              onPressed: () {
                actions.elementAt(index).onTap();
                _animationController.reverse();
              },
              mini: widget.mini ?? true,
              child: actions.elementAt(index).child,
            ),
          ),
        );
      }).toList()
        ..add(FloatingActionButton(
          //heroTag: null,
          backgroundColor: color,
          onPressed: () {
            if (_animationController.isDismissed) {
              _animationController.forward();
            } else {
              _animationController.reverse();
            }
          },
          child: new AnimatedBuilder(
            animation: _animationController,
            builder: (BuildContext context, Widget child) {
              return new Transform(
                transform: new Matrix4.rotationZ(
                  _animationController.value * 0.5 * math.pi,
                ),
                alignment: FractionalOffset.center,
                child: _animationController.isDismissed
                    ? widget.dismissed
                    : widget.active,
              );
            },
          ),
        )),
    );
  }
}
