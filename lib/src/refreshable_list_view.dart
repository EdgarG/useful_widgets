import 'package:flutter/material.dart';

/// This class is a wrapper around [ListView] with [RefreshIndicator]
/// If the [onRefresh] is null it return a normal list view
class RefreshableListView<T> extends StatefulWidget {
  final List<T> items;
  final Widget Function(BuildContext context) noItemBuilder;
  final Widget Function(BuildContext context, T item) itemBuilder;
  final Future<List<T>> Function() onRefresh;
  final Widget divider;

  const RefreshableListView({
    Key key,
    @required this.items,
    @required this.itemBuilder,
    this.noItemBuilder,
    this.onRefresh,
    this.divider,
  })  : assert(items != null),
        assert(itemBuilder != null),
        super(key: key);

  @override
  _RefreshableListViewState<T> createState() => _RefreshableListViewState<T>();
}

class _RefreshableListViewState<T> extends State<RefreshableListView<T>> {
  GlobalKey<RefreshIndicatorState> refreshIndicatorKey;
  bool isLoading = false;
  List<T> items;

  @override
  void initState() {
    items = widget.items;
    if (widget.onRefresh != null) {
      refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (items == null || items.isEmpty) {
      return getEmptyItems();
    }

    return getItemsList();
  }

  Widget getEmptyItems() {
    Widget list = ListView(
      shrinkWrap: true,
      children: <Widget>[widget.noItemBuilder(context)],
    );

    if (widget.onRefresh == null) {
      return Container(
        alignment: Alignment.center,
        child: list,
      );
    }

    return Container(
      alignment: Alignment.center,
      child: RefreshIndicator(
        key: refreshIndicatorKey,
        onRefresh: () async => widget.onRefresh(),
        child: list,
      ),
    );
  }

  Widget getItemsList() {
    if (widget.onRefresh == null) {
      return getItems(items);
    }

    return RefreshIndicator(
      key: refreshIndicatorKey,
      onRefresh: () async {
        var result = await widget.onRefresh();
        setState(() {
          items = result;
        });
      },
      child: getItems(items),
    );
  }

  Widget getItems(List<T> items) {
    return ListView.separated(
      itemCount: items.length,
      itemBuilder: (context, index) =>
          widget.itemBuilder(context, items[index]),
      separatorBuilder: (c, i) => widget.divider ?? Container(),
    );
  }
}
