import 'package:flutter/material.dart';

class BottomSheetItem extends StatelessWidget {
  final Widget title;
  final Widget leading;
  final Widget trailing;
  final Function onTap;

  const BottomSheetItem({
    Key key,
    this.leading,
    this.trailing,
    @required this.onTap,
    @required this.title,
  })  : assert(title != null),
        assert(onTap != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: leading,
      title: title,
      trailing: trailing,
      onTap: () {
        Navigator.pop(context);
        onTap();
      },
    );
  }
}

class BottomSheetDialog extends StatelessWidget {
  final List<BottomSheetItem> items;

  const BottomSheetDialog({
    Key key,
    this.items,
  })  : assert(items != null),
        assert(items.length > 0),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      children: items,
    );
  }

  static Future show({
    @required BuildContext context,
    @required BottomSheetDialog dialog,
    Color backgroundColor,
    bool isDismissible = true,
    ShapeBorder shape,
  }) async {
    assert(dialog != null);

    await showModalBottomSheet(
      context: context,
      builder: (context) {
        return dialog;
      },
      backgroundColor: backgroundColor,
      isDismissible: isDismissible,
      shape: shape,
    );
  }
}
