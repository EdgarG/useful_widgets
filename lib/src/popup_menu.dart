import 'package:flutter/material.dart';

/// A [PopupMenu] Item with [onTap] callback that is triggered when
/// the [PopupMenuButton] function [onSelected] is triggered
///
class PopupItem<T> extends StatelessWidget {
  final T value;
  final Widget child;
  final Function onTap;

  const PopupItem({
    Key key,
    this.value,
    @required this.onTap,
    @required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return child;
  }
}

class TextPopupItem extends PopupItem<String> {
  final Icon icon;

  const TextPopupItem({
    Key key,
    @required this.icon,
    @required String value,
    @required Function onTap,
  }) : super(key: key, value: value, onTap: onTap, child: null);

  @override
  Widget build(BuildContext context) {
    return PopupItem<String>(
      value: value,
      onTap: onTap,
      child: Row(
        children: <Widget>[
          icon,
          Text(value),
        ],
      ),
    );
  }
}

class PopupMenu<T> extends StatelessWidget {
  final List<PopupItem<T>> items;
  final Offset offset;
  final double elevation;
  final Function onCancel;

  const PopupMenu({
    Key key,
    @required this.items,
    this.offset,
    this.elevation,
    this.onCancel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<int>(
      elevation: elevation ?? 2.0,
      offset: offset ?? Offset(0, 40),
      itemBuilder: (BuildContext context) {
        return items
            .asMap()
            .map((index, item) {
              return MapEntry(
                index,
                PopupMenuItem<int>(
                  value: index,
                  child: item,
                ),
              );
            })
            .values
            .toList();
      },
      onCanceled: () => onCancel != null ? onCancel() : null,
      onSelected: (int choiceIndex) => items[choiceIndex].onTap(),
    );
  }
}
